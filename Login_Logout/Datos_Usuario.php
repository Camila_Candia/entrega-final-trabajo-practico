<?php

    try
    {
      //conexión a la base de datos    
      $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');

      //captura de los datos ingresados por el usuario
      $Nombre= $_POST['Nombre'];
      $Apellido= $_POST['Apellido'];
      $Usuario= $_POST['Usuario'];
      $Contraseña= $_POST['Contraseña'];
      $hoy = date("Y:m:d:h:i:sa");

      //encriptación de la contraseña
      $randomBytes = random_bytes(22);
      $salt = bin2hex($randomBytes);

      $opciones =['cost'=> 16,
                  'salt'=> $salt, 
                  ];

      $contraseña_encriptada= password_hash($Contraseña,PASSWORD_BCRYPT,$opciones);

      //inserción de los datos a la base de datos 
      $consulta="insert into usuarios(nombre, apellido, usuario, contraseña,fecha_creacion) values(?,?,?,?,?)";
      $sql=$conexion -> prepare($consulta);
      
      $sql->bindParam(1,$Nombre);
      $sql->bindParam(2,$Apellido);
      $sql->bindParam(3,$Usuario);
      $sql->bindParam(4,$contraseña_encriptada);
      $sql->bindParam(5,$hoy);

      $sql->execute();

    }
    //notifica si es que hay algún error
    catch(PDOException $error)
    {
      echo ' El error es el siguiente: $error';
    }

    //cuando se inserten todos los datos redirecciona a la página principal
    header("location:../index.php");

?>
