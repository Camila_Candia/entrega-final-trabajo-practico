<?php

    //iniciamos la sesion
    session_start();

    //borramos la variable de sesion
    unset($_SESSION['usuario']);

    //borramos/cerramos la sesion
    session_destroy();

    //redirecciona al login
    header("Location: Login.php");
    
?>
