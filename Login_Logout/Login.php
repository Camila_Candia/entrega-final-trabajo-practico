<!DOCTYPE html>
<html>

   <head>

      <meta charset="utf-8">
      <!--Título de la página. Es el que aparece en la pestaña del navegador-->
      <title>Login</title>
      <!--Llama a los estilos que se deben aplicar en esta ventana--> 
      <link rel="stylesheet" type="text/css" href="prueba.css"> 
   
   </head>

   <body>

      <div class="overlay">
         
         <form action="Datos_Login.php" method="POST">
            
            <div class="con">
               
               <header class="head-form">
                  <h2>Iniciar Sesión</h2>
                  <p>Ingrese su nombre y contraseña</p>
               </header>
            
               <br>
               <div class="field-set">
                  <!--Nombre de usurario-->
                  <input class="form-input" id="txt-input" type="text" placeholder="Nombre" name="Usuario" required>
                  <br>
               
                  <!--Contraseña -->
                  <input class="form-input" type="password" placeholder="Contraseña" id="pwd"  name="Contraseña" required>
                  <br>

                  <button class="log-in"> Conectarse </button>
               </div>
            
            </div>

               <!--Enlace a Registrar nuevo usuario-->
             <a href="Registrar_Nuevo_Usuario.php"> ¿No tienes una cuenta? </a>
         </form>

      
      </div>
   </body>
</html>
