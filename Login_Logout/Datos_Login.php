<?php

    //inciar sesión 
    session_start();
    
    //conexión a la base de datos 
    $conexion= new PDO('pgsql:host=127.0.0.1;dbname=SCM;','postgres' , 'infamesrp');
    
    $contador=0;

    //captura de los datos que el usuario ingresa
    $Usuario=$_POST['Usuario'];
    $Contraseña=$_POST['Contraseña'];
    
    //selecciona los datos de la base de datos y lo coloca dentro de un marcador 
    $consulta="select usuario,contraseña from usuarios where usuario = :login";
    $sql=$conexion->prepare($consulta);
    
    //ejecuta la consulta y busca el nombre del usuario que se introdujo en el formulario
    $sql->execute(array(":login"=>$Usuario));
    
    //coloca todo en un array asociativo
    while($registro=$sql->fetch(PDO::FETCH_ASSOC))
    {

        //preguntamos si la contraseña que el usuario introdujo es igual a la que esta en la base de datos
        if (password_verify($Contraseña,$registro['contraseña']))
        {

          //se aumenta el contador si las contraseñas son iguales 
          $contador++;
         
        }

    }
    
    //si el contador es mayor que 0 redirecciona a la página del menú 
    if ($contador>0)
    {

        //se guarda el usuario en una variable de sesion 
        $_SESSION['usuario']=$Usuario;
        header("Location:../index.php");

    }

    //si el contador no aumentó imprime el siguiente mensaje 
    else 
    {

      echo "El usuario no existe";

    }  
     
?>
